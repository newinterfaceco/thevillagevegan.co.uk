import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Image from "../components/image"
import Header from "../components/header"



  const Contact = ({ children }) => {

    return (
      <>

        <Header siteTitle={'The Village Vegan'} />

        <Layout>

          <SEO title="Contact" />

          <div style={{ maxWidth: `500px`, margin: `100px auto 0` }}>
              <a href="mailto:hello@thevillagevegan.co.uk" target="_blank" rel="noopener noreferrer">
                  <Image alt="Email Us" filename="email.jpg" />
              </a>
          </div>

        </Layout>

      </>
    )
  }

export default Contact
