import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Image from "../components/image"
import Header from "../components/header"

  const About = ({ children }) => {

    return (
      <>


<Header siteTitle={'The Village Vegan'} />
        <Layout>
            
          <SEO title="Contact" />

           <div style={{ maxWidth: `500px`, margin: `100px auto` }}>
                <Image alt="Hello" filename="hello.jpg" />
            </div>

            <div className="squiggle"></div>

            <div style={{ maxWidth: `500px`, margin: `80px auto 100px` }}>

                <h3>Next Event</h3>
                <p>Cottenham Village Green</p>
                <p>Saturday 28th March 2020</p>
                <p>10:30am - 4pm</p>
                <p>This event is FREE of charge, no ticket required.</p>

            </div>

            <div class="squiggle"></div>

            <div style={{ maxWidth: `500px`, margin: `80px auto 100px` }}>

                <p>Cambridgeshire based, The Village Vegan has been made to host local businesses at a fair price and make vegan products accessible in the wider community.</p>
                <p>The market is a place of inclusion, where everyone should feel safe.</p>
                <p>Any business can attend however only vegan products are to be sold during the markets duration.</p>
                <p>Application forms to trade available via email, or messaging via our Facebook or Instagram.</p>

                <p>Thank you! The Village Vegan.</p>

            </div>

            <div class="squiggle"></div>

            <div style={{ maxWidth: `500px`, margin: `60px auto` }}>
                <a href="mailto:hello@thevillagevegan.co.uk" target="_blank" rel="noopener noreferrer">
                    <Image alt="Email Us" filename="email.jpg" />
                </a>
            </div>
          </Layout>
      </>
    )
  }

export default About
