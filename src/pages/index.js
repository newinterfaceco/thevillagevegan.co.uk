import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"


const IndexPage = () => (
  <Layout>
    <SEO title="About" />
    <div style={{ maxWidth: `500px`, margin: `50px auto 60px` }}>
        <Image alt="Hello" filename="logo.jpg" />
    </div>

    <div class="squiggle"></div>

    <div style={{ maxWidth: `500px`, margin: `40px auto 60px auto` }}>

      <Link to='/about' className={`btn`}>
        About &amp; Events
      </Link>

      <Link to='/contact' className={`btn`}>
        Gallery &amp; Contact
      </Link>

    </div>

  </Layout>
)

export default IndexPage
